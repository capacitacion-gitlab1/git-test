/Le damos un nombre "MiClase" a la clase
public class MiClase
{
    //Atributos de la clase
    private String atributo1;
    private int atributo2;
    private float atributo3;

    //Constructor con el mismo nombre de la clase
    public MiClase(){
        System.out.println("Hola mundo jaja");
    }

    //Métodos de la clase
    public void metodo1()
    {
        //Método vacío
    }

    public String metodo2()
    {
        return "metodo2";
    }
}
